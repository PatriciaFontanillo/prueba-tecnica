<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       patriciafontanillo.es
 * @since      1.0.0
 *
 * @package    Plugin_01
 * @subpackage Plugin_01/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Plugin_01
 * @subpackage Plugin_01/public
 * @author     Patricia <patricia.alvarez.fontanillo@gmail.com>
 */
class Plugin_01_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_01_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_01_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/plugin-01-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_01_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_01_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/plugin-01-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add literal to the end of the title
	 *
	 * @since    1.0.0
	 */
	public function change_title_meta($title){

		if(is_page()){
			$literal = ' - Flat101';
			$title['title'] .= $literal;
		}

		return $title;

	}

	/**
	 * Add literal to the end of the title
	 *
	 * @since    1.0.0
	 */
	public function change_title_frontend($title){

		if(is_page() && in_the_loop() && !is_archive() ){ // This will skip the menu items and the archive titles and not pages type
			$literal = ' - Flat101';
			$title .= $literal;
		}

		return $title;

	}

	/**
	 * Add og:image on post
	 *
	 * @since    1.0.0
	 */

	public function add_custom_meta(){

		if( is_single() ) {

			global $post;

			if(has_post_thumbnail( $post->ID )) { 
				$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
				echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
			}

		}
	}


}
