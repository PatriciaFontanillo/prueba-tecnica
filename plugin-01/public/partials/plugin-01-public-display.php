<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       patriciafontanillo.es
 * @since      1.0.0
 *
 * @package    Plugin_01
 * @subpackage Plugin_01/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
