<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       patriciafontanillo.es
 * @since      1.0.0
 *
 * @package    Plugin_02
 * @subpackage Plugin_02/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_02
 * @subpackage Plugin_02/admin
 * @author     Patricia <patricia.alvarez.fontanillo@gmail.com>
 */
class Plugin_02_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_02_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_02_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/plugin-02-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_02_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_02_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/plugin-02-admin.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * Create custom post (custom post shop) 
	 *
	 * @since    1.0.0
	 */
	public function custom_post_shop(){
		register_post_type( 'shop',
			array(
				'labels' => array(
					'name' => __( 'Tiendas' ),
					'singular_name' => __( 'Tienda' ),
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => 'tienda'),
				'show_in_rest' => true,
				'supports' => array('title')
	 
			)
		);
	}

	/**
	 * Register custom meta box (custom post shop) 
	 *
	 * @since    1.0.0
	 */
	public function custom_post_shop_meta_boxes(){
		add_meta_box('shop_data_meta_box', 'Datos de la tienda', array($this,'shop_data_meta_box'), 'shop', 'normal','high' );
	}

	/**
	 * Add custom meta (custom post shop) 
	 *
	 * @since    1.0.0
	 */
	public function shop_data_meta_box($post){
		// Add a nonce field so we can check for it later.
		wp_nonce_field( $this->plugin_name.'_shop_meta_box', $this->plugin_name.'_shop_meta_box_nonce' );

		echo '<div id="shop_field_container">';
		echo '<ul class="plugin_name_product_data_metabox">';
		$description = get_post_meta( $post->ID, $this->plugin_name.'_description', true );
		$args = array(
		'textarea_name' => $this->plugin_name.'_description',
		); 
		echo '<li class="list" ><label class="title-label" for="'.$this->plugin_name.'_description">';
		_e( 'Descripción', $this->plugin_name.'_description' );
		echo '</label>';
		wp_editor( $description, $this->plugin_name.'_description_editor',$args); 
		echo '</li></ul>';

		echo '<li class="list"><label class="title-label" for="'.$this->plugin_name.'_direction_name">';
		_e( 'Dirección', $this->plugin_name.'_direction_name' );
		echo '</label>';
		$args = array (
	              'type'      => 'input',
				  'subtype'	  => 'text',
				  'id'	  => $this->plugin_name.'_direction_name',
				  'name'	  => $this->plugin_name.'_direction_name',
				  'required' => '',
				  'get_options_list' => '',
				  'value_type'=>'normal',
				  'wp_data' => 'post_meta',
				  'post_id'=> $post->ID
	          );
		// this gets the post_meta value and echos back the input
		$this->plugin_name_render_settings_field($args);
		echo '</li></div>';
	}

	/**
	 * Render custom fields 
	 *
	 * @since    1.0.0
	 */
	public function plugin_name_render_settings_field($args) {
		if($args['wp_data'] == 'option'){
			$wp_data_value = get_option($args['name']);
		} elseif($args['wp_data'] == 'post_meta'){
			$wp_data_value = get_post_meta($args['post_id'], $args['name'], true );
		}
		
		switch ($args['type']) {
			case 'input':
				$value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
				if($args['subtype'] != 'checkbox'){
					$prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">'.$args['prepend_value'].'</span>' : '';
					$prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
					$step = (isset($args['step'])) ? 'step="'.$args['step'].'"' : '';
					$min = (isset($args['min'])) ? 'min="'.$args['min'].'"' : '';
					$max = (isset($args['max'])) ? 'max="'.$args['max'].'"' : '';
					if(isset($args['disabled'])){
						// hide the actual input bc if it was just a disabled input the informaiton saved in the database would be wrong - bc it would pass empty values and wipe the actual information
						echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'_disabled" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="'.$args['id'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
					} else {
						echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
					}
					
				} else {
					$checked = ($value) ? 'checked' : '';
					echo '<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" name="'.$args['name'].'" size="40" value="1" '.$checked.' />';
				}
				break;
			default:
				break;
		}
	}

	/**
	 * Save custom fields 
	 *
	 * @since    1.0.0
	 */
	public function custom_post_shop_meta_boxex_save( $post_id ) {

		/*
		 * We need to verify this came from our screen and with proper authorization,
		 * because the save_post action can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST[$this->plugin_name.'_shop_meta_box_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST[$this->plugin_name.'_shop_meta_box_nonce'], $this->plugin_name.'_shop_meta_box' ) ) {
			var_dump($_POST);
			die();
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		
		// Make sure that it is set.
		if ( !isset( $_POST[$this->plugin_name.'_direction_name'] ) && !isset( $_POST[$this->plugin_name.'_description']  )) {
			return;
		}

		// Sanitize user input.
		$direction_name = sanitize_text_field( $_POST[$this->plugin_name."_direction_name"]);
		$description = sanitize_text_field( $_POST[$this->plugin_name."_description"]);
	
		update_post_meta($post_id, $this->plugin_name.'_direction_name',$direction_name);
		update_post_meta($post_id, $this->plugin_name.'_description',$description);
	
	}

	/**
	 * Add custom fields to API REST (custom post shop) 
	 *
	 * @since    1.0.0
	 */
	public function add_custom_fields() {
		register_rest_field ( 'shop', 'custom_fields', array(
			'get_callback'    => array($this,'get_post_meta_for_api'),
			'schema'          => null,
		 ) );
	}

	/**
	 * Callback -  add_custom_fields() return custom meta
	 *
	 * @since    1.0.0
	 */
	public function get_post_meta_for_api( $object ) {
		$post_id = $object['id'];
		return get_post_meta( $post_id );
	}

}
