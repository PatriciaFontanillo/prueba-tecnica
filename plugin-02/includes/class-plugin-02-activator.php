<?php

/**
 * Fired during plugin activation
 *
 * @link       patriciafontanillo.es
 * @since      1.0.0
 *
 * @package    Plugin_02
 * @subpackage Plugin_02/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_02
 * @subpackage Plugin_02/includes
 * @author     Patricia <patricia.alvarez.fontanillo@gmail.com>
 */
class Plugin_02_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
