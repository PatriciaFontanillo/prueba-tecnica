var url = "http://pruebaswp.com/wp-json/wp/v2/shop";

function loadShops() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
           if (xmlhttp.status == 200) {
                let data =JSON.parse(xmlhttp.responseText);
                appendData(data);
           }
           else if (xmlhttp.status == 400) {
              alert('There was an error 400');
           }
           else {
               alert('Something else other than 200 was returned');
           }
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function appendData(data) {
    var mainContainer = document.getElementById("shops");
    var loader = document.getElementById("shops_loader");
    var div = document.createElement("div");

    div.className = "shops_el";

    if(data.length == 0){
        div.innerHTML = 'No hay tiendas disponibles';
        mainContainer.appendChild(div);
    }else{
        for (var i = 0; i < data.length; i++) {
            var div = document.createElement("div");
            div.className = "shops_el";
            div.innerHTML = 'ID: ' + data[i].id + '</br>' + 
                            'Nombre: ' + data[i].title.rendered + '</br>' + 
                            'Descripción: ' + data[i].custom_fields['plugin-02_description'][0] + '</br>' + 
                            'Dirección: ' + data[i].custom_fields['plugin-02_direction_name'][0] 
            mainContainer.appendChild(div);
        }
    }

    // Hidden loader
    loader.style.display = "none";
}


window.addEventListener("load", function(){
    loadShops();
});